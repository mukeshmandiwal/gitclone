import React from 'react'
import {connect} from 'react-redux'
import styled from 'styled-components'
import {fetchOverviewDetails, fetchUserDetails} from 'redux/actions'
import Home from './Home'
const AppContainer = styled.div`
  width: 100%;
  height: 100vh;
  overflow-y: auto;
`

const App = props => {
  React.useEffect(() => {
    props.userDetails()
    props.overviewDetails()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])
  return (
    <AppContainer>
      <Home></Home>
    </AppContainer>
  )
}

const mapDispatchToProps = dispatch => {
  return {
    userDetails: () => dispatch(fetchUserDetails()),
    overviewDetails: () => dispatch(fetchOverviewDetails()),
  }
}
export default connect(
  null,
  mapDispatchToProps,
)(App)
