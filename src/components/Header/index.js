import React from 'react'
import styled from 'styled-components'
const Wrapper = styled.div`
  height: 60px;
  width: 100%;
  position: fixed;
  z-index: 1;
  background: #fff;
  display: flex;
  align-items: center;
  @media only screen and (max-width: 700px) {
    display: none;
    height: 0;
  }
`
const Text = styled.div`
  height: 100%;
  display: flex;
  margin-right: 25px;
  align-items: center;
`
const LeftSection = styled.div`
  width: 22%;
`
const RightSection = styled.div`
  width: 78%;
  height: 100%;
  display: flex;
  align-items: center;
  padding: 0 2%;
`
const Header = props => {
  return (
    <Wrapper>
      <LeftSection></LeftSection>
      <RightSection>
        <Text>Overview</Text>
        <Text style={{fontWeight: '600', borderBottom: '2px solid red'}}>
          Repositories
        </Text>
        <Text>Project</Text>
        <Text>Packages</Text>
        <Text>Stars</Text>
        <Text>Followers</Text>
        <Text>Following</Text>
      </RightSection>
    </Wrapper>
  )
}

export default Header
