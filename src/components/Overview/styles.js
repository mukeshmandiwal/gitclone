import styled from 'styled-components'
export const OverViewContainer = styled.div`
  width: 78%;
  padding: 0 2%;
  display: flex;
  flex-direction: column;
  align-items: center;
  color: rgba(65, 65, 65, 1);
  @media only screen and (max-width: 700px) {
    width: 98%;
    justify-content: center;
    padding: 1%;
  }
`

export const RepoDetails = styled.div`
  height: 120px;
  width: 100%;
  position: relative;
  padding: 20px 0;
  color: rgba(65, 65, 65, 0.7);
  border-top: 1px solid #e1e4e8;
  @media only screen and (max-width: 700px) {
    height: auto;
  }
`

export const NameText = styled.div`
  color: #0366d6;
  font-size: 20px;
  padding: 10px 0;
  font-weight: 600;
`

export const TextWrapper = styled.span`
  display: flex;
  align-items: center;
`

export const Text = styled.div`
  font-size: 14px;
  font-weight: 500;
  padding-right: 15px;
`
export const Form = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  line-height: 38px;
  border: ${props =>
    props.inputFocus ? '2px solid blue' : '1px solid #dfdfdf'};
  border-radius: 3px;
  background-color: #fff;
  width: 70%;
  height: 40px;
  padding: 0 15px;
  cursor: pointer;
  border-radius: 3px;
  z-index: 999;
  transition: all 300ms cubic-bezier(0.645, 0.045, 0.355, 1);
  @media only screen and (max-width: 700px) {
    width: 100%;
  }
`

export const Input = styled.input`
  line-height: 1;
  background-color: transparent;
  width: 100%;
  margin-left: ${props => (props.barOpened ? '1rem' : '0rem')};
  border: none;
  color: ${props => props.theme.fontColorPrimary};
  font-size: ${props => props.theme.font16};
  transition: all 300ms cubic-bezier(0.645, 0.045, 0.355, 1);
  &:focus,
  &:active {
    outline: red;
  }
  &::placeholder {
    color: ${props => props.theme.fontColorPrimary};
    font-size: ${props => props.theme.font16};
  }
`

export const FilterContainer = styled.div`
  display: flex;
  height: 60px;
  width: 100%;
  padding: 0 10px;
  position: relative;
  align-items: center;
  border-top: 1px solid rgba(27, 31, 35, 0.2);
  @media only screen and (max-width: 700px) {
    flex-direction: column;
    height: auto;
    padding: 10px;
  }
`
export const Button = styled.div`
  display: flex;
  cursor: pointer;
  display: flex;
  justify-content: center;
  font-weight: 600;
  background: #eff3f6;
  cursor: pointer;
  font-size: 12px;
  padding: 0 20px;
  display: flex;
  border-radius: 2px;
  margin: 10px;
  border: 1px solid rgba(27, 31, 35, 0.2);
  justify-content: center;
  padding: 8px;
  &:hover {
    background: #ccc;
  }
`

export const StarButton = styled(Button)`
  position: absolute;
  align-items: center;
  right: 3%;
  padding: 5px 9px;
  justify-content: space-around;
  @media only screen and (max-width: 700px) {
    right: 0%;
    padding: 2px 9px;
  }
`

export const ButtonNew = styled(Button)`
  align-items: center;
  right: 3%;
  padding: 5px 9px;
  background: #28a745;
  color: #fff;
  &:hover {
    background: #28a745;
  }
`
export const ButtonSection = styled.div`
  position: relative;
  display: flex;
`
export const BtnWrapper = styled.div`
  display: flex;
  @media only screen and (max-width: 700px) {
    width: 100%;
  }
`

export const DetailsSection = styled.div`
  display: flex;
  padding-top: 10px;
  align-items: center;
`
