import React from 'react'
import formatDate from 'utils/util'
import {LanguageIcon, StarIcon, NewIcon} from 'shared/Icons'
import DropDown from 'shared/DropDown'
import * as S from './styles'
const Overview = props => {
  const {overviewDetails} = props
  const [reposList, setRepoList] = React.useState([])
  const [isPlanModal, setModal] = React.useState(false)
  const [isType, setIsType] = React.useState()
  function toggleModal(status) {
    setModal(!isPlanModal)
  }
  React.useEffect(() => {
    setRepoList(overviewDetails)
  }, [overviewDetails])
  function filterHandler(value) {
    setIsType(value)
    toggleModal()
  }

  function searchHandler(e) {
    var updatedList = overviewDetails
    updatedList = updatedList.filter(function(item) {
      return item.name.toLowerCase().search(e.target.value.toLowerCase()) !== -1
    })
    setRepoList(updatedList)
  }
  return (
    <React.Fragment>
      {reposList && (
        <S.OverViewContainer>
          <S.FilterContainer>
            <S.Form inputFocus={''}>
              <S.Input
                onChange={searchHandler}
                placeholder="Find a repository..."
              />
            </S.Form>
            <S.BtnWrapper>
              <S.ButtonSection>
                <S.Button onClick={() => filterHandler('type')}>Type</S.Button>
                <S.Button onClick={() => filterHandler('language')}>
                  Language
                </S.Button>
                {isPlanModal && (
                  <DropDown
                    id="modal"
                    type={isType}
                    isModalOpen={isPlanModal}
                    onClose={toggleModal}
                    modalSize="md"
                    {...props}
                  ></DropDown>
                )}
              </S.ButtonSection>
              <S.ButtonNew>
                <NewIcon size="20"></NewIcon>
                <span>New</span>
              </S.ButtonNew>
            </S.BtnWrapper>
          </S.FilterContainer>
          {reposList.map((value, index) => {
            return (
              <S.RepoDetails key={index}>
                <S.StarButton>
                  <StarIcon />
                  <span style={{paddingLeft: '5px'}}>Star</span>
                </S.StarButton>
                <S.NameText>{value.name}</S.NameText>
                <S.Text>{value.description}</S.Text>
                <S.DetailsSection>
                  {value.language && (
                    <S.TextWrapper>
                      <LanguageIcon color={value.language} />
                      <S.Text> {value.language}</S.Text>
                    </S.TextWrapper>
                  )}
                  {value.license && <S.Text>{value.license.name}</S.Text>}
                  <S.Text>
                    Updated on: {formatDate(new Date(value.updated_at))}
                  </S.Text>
                </S.DetailsSection>
              </S.RepoDetails>
            )
          })}
        </S.OverViewContainer>
      )}
    </React.Fragment>
  )
}

export default Overview
