import React from 'react'
import {connect} from 'react-redux'
import styled from 'styled-components'
import Header from '../Header'
import Profile from '../Profile'
import Overview from '../Overview'

const HomeContainer = styled.div`
  display: flex;
  overflow-y: auto;
  width: 100%;
  flex-direction: column;
  @media only screen and (max-width: 700px) {
    flex-direction: column;
  }
`

const MainContainer = styled.div`
  display: flex;
  overflow-y: auto;
  width: 100%;
  margin-top: 60px;
  @media only screen and (max-width: 700px) {
    flex-direction: column;
    margin-top: 20px;
  }
`

const Home = props => {
  return (
    <HomeContainer>
      <Header />
      <MainContainer>
        <Profile {...props}></Profile>
        <Overview {...props}></Overview>
      </MainContainer>
    </HomeContainer>
  )
}

const mapStateToProps = state => {
  return {
    userDetails: state.repo.userDetails,
    overviewDetails: state.repo.overviewDetails,
  }
}

export default connect(
  mapStateToProps,
  null,
)(Home)
