import React from 'react'
import styled from 'styled-components'
import {LocationIcon, CompanyIcon, EmailIcon} from 'shared/Icons'
const ProfileContainer = styled.div`
  width: 22%;
  display: flex;
  flex-direction: column;
  align-items: center;
  color: rgba(65, 65, 65, 1);
  @media only screen and (max-width: 700px) {
    width: 100%;
    align-items: flex-start;
  }
`

const ImpageWrapper = styled.div`
  display: flex;
  flex-direction: column;
  @media only screen and (max-width: 700px) {
    flex-direction: row;
    width: 100%;
    padding-left: 10px;
  }
`

const ImgSection = styled.div`
  width: 260px;
  height: 260px;
  @media only screen and (max-width: 700px) {
    width: 80px;
    height: 80px;
  }
`

const ImgSrc = styled.img`
  width: 260px;
  height: 260px;
  @media only screen and (max-width: 700px) {
    width: 80px;
    height: 80px;
  }
`

const TextView = styled.span`
  display: flex;
  align-items: center;
`

const NameText = styled.span`
  font-weight: 600;
  font-size: 26px;
  line-height: 30px;
`

const UserName = styled.span`
  font-weight: 600;
  font-size: 16px;
  color: rgba(65, 65, 65, 0.6);
`

const Text = styled.span`
  font-size: 12px;
  font-weight: 600;
  color: rgba(65, 65, 65, 0.8);
`

const DetailsSection = styled.span`
  display: flex;
  width: 260px;
  flex-direction: column;
  @media only screen and (max-width: 700px) {
    width: 100%;
    padding: 10px;
  }
`

const BioInfo = styled(DetailsSection)`
  @media only screen and (max-width: 700px) {
    width: 100%;
    padding-left: 12px;
  }
`

const Button = styled.div`
  display: flex;
  cursor: pointer;
  display: flex;
  width: 100%;
  justify-content: center;
  font-weight: 600;
  background: #eff3f6;
  cursor: pointer;
  font-size: 12px;
  display: flex;
  border-radius: 4px;
  margin: 10px 0;
  border: 1px solid rgba(27, 31, 35, 0.2);
  justify-content: center;
  padding: 8px;
  &:hover {
    background: #ccc;
  }
  @media only screen and (max-width: 700px) {
    display: none;
  }
`

const Profile = props => {
  const {userDetails} = props
  return (
    <ProfileContainer>
      {props.userDetails && (
        <React.Fragment>
          <ImpageWrapper>
            <ImgSection>
              <ImgSrc src={userDetails.avatar_url} alt="profile_avatar" />
            </ImgSection>
            <BioInfo>
              <NameText>{userDetails.name}</NameText>
              <UserName>{userDetails.login}</UserName>
              <Button>Edit Profile</Button>
            </BioInfo>
          </ImpageWrapper>
          <DetailsSection>
            <Text>{userDetails.bio}</Text>
            {userDetails.company && (
              <TextView>
                <CompanyIcon />
                <Text>{userDetails.company}</Text>
              </TextView>
            )}
            {userDetails.location && (
              <TextView>
                <LocationIcon />
                <Text>{userDetails.location}</Text>
              </TextView>
            )}
            {userDetails.email && (
              <TextView>
                <EmailIcon></EmailIcon>
                <Text>{userDetails.email}</Text>
              </TextView>
            )}
          </DetailsSection>
        </React.Fragment>
      )}
    </ProfileContainer>
  )
}

export default Profile
