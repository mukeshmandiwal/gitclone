import React from 'react'
import ReactDOM from 'react-dom'
import {BrowserRouter} from 'react-router-dom'
import {Provider} from 'react-redux'
import {store} from 'redux/store'
import 'shared/styles/index.css'
import App from 'components/App'
import * as serviceWorker from './serviceWorker'

const app = (
  <BrowserRouter>
    <Provider store={store}>
      <App />
    </Provider>
  </BrowserRouter>
)
ReactDOM.render(app, document.getElementById('root'))
serviceWorker.unregister()
