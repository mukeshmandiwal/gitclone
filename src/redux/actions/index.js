import * as actionTypes from './actionTypes'
export const fetchUserDetails = () => ({
  type: actionTypes.REQUEST_USER_DETAILS,
})

export const fetchOverviewDetails = (lType, language) => ({
  type: actionTypes.REQUEST_OVERVIEW_DETAILS,
  lType,
  language,
})
