import * as actionType from '../../actions/actionTypes'
const initialState = {
  userLoader: false,
  overviewLoader: false,
}
export const repoReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.USER_DETAILS_LOADING:
      return {
        ...state,
        userLoader: true,
      }
    case actionType.USER_DETAILS:
      return {
        ...state,
        userLoader: false,
        userDetails: action.payload,
      }
    case actionType.LOADING_OVERVIEW_DETAILS:
      return {
        ...state,
        overviewLoader: false,
      }
    case actionType.OVERVIEW_DETAILS:
      return {
        ...state,
        overviewLoader: true,
        overviewDetails: action.payload,
      }
    default:
      return state
  }
}
