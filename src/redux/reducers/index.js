import {combineReducers} from 'redux'
import {repoReducer} from './reducer'

export default combineReducers({
  repo: repoReducer,
})
