import {takeLatest, all, fork} from 'redux-saga/effects'
import * as actionTypes from '../actions/actionTypes'
import {getUserDetails, getOverviewDetails} from './repos'

function* watchRepoRequest() {
  yield takeLatest(actionTypes.REQUEST_USER_DETAILS, getUserDetails)
  yield takeLatest(actionTypes.REQUEST_OVERVIEW_DETAILS, getOverviewDetails)
}

export function* rootSaga() {
  yield all([fork(watchRepoRequest)])
}
