import axios from 'utils/axios.api'
import {put} from 'redux-saga/effects'
import * as actionType from '../../actions/actionTypes'

export function* getUserDetails(action) {
  try {
    yield put({
      type: actionType.USER_DETAILS_LOADING,
    })
    const response = yield axios.get(`supreetsingh247`)
    yield put({
      type: actionType.USER_DETAILS,
      payload: response.data,
    })
  } catch (error) {}
}

export function* getOverviewDetails(action) {
  const params = {
    tab: 'repositories',
    type: action.lType,
    language: action.language,
  }
  try {
    yield put({
      type: actionType.LOADING_OVERVIEW_DETAILS,
    })
    const response = yield axios.get(`supreetsingh247/repos`, {
      params,
    })
    yield put({
      type: actionType.OVERVIEW_DETAILS,
      payload: response.data,
    })
  } catch (error) {}
}
