import React, {useState, useEffect, useRef} from 'react'
import {connect} from 'react-redux'
import {fetchOverviewDetails} from 'redux/actions'
import * as M from './styles'

export const typeList = [
  {id: 'type', title: 'All', selected: false, key: true},
  {id: 'type', title: 'Public', selected: false, key: false},
  {id: 'type', title: 'Private', selected: false, key: false},
  {id: 'type', title: 'Sources', selected: false, key: false},
  {id: 'type', title: 'Forks', selected: false, key: false},
  {id: 'type', title: 'Archived', selected: false, key: false},
  {id: 'type', title: 'Mirrors', selected: false, key: false},
]

export const languageList = [
  {id: 'type', title: 'All', selected: false, key: true},
  {id: 'type', title: 'JavaScript', selected: false, key: false},
  {id: 'type', title: 'TypeScript', selected: false, key: false},
  {id: 'type', title: 'Java', selected: false, key: false},
  {id: 'type', title: 'CoffeeScript', selected: false, key: false},
  {id: 'type', title: 'CSS', selected: false, key: false},
  {id: 'type', title: 'HTML', selected: false, key: false},
]

const DropDown = props => {
  const [fadeType, setState] = useState(null)
  const background = useRef()

  useEffect(() => {
    window.addEventListener('keydown', onEscKeyDown, false)
    setTimeout(() => setState('in'), 0)
    return () => {
      window.removeEventListener('keydown', onEscKeyDown, false)
    }
  }, [props.isOpen])

  const transitionEnd = e => {
    if (e.propertyName !== 'opacity' || fadeType === 'in') return
    if (fadeType === 'out') {
      props.onClose()
    }
  }

  const onEscKeyDown = e => {
    if (e.key !== 'Escape') return
    setState('out')
  }

  const handleClick = e => {
    e.preventDefault()
    setState('out')
  }

  return (
    <M.Modal
      id={props.id}
      className={`wrapper ${'size-' + props.modalSize} fade-${fadeType} ${
        props.modalClass
      }`}
      role="dialog"
      left={props.left}
      right={props.right}
      modalSize={props.modalSize}
      onTransitionEnd={transitionEnd}
    >
      <M.BoxDialog>
        <M.BoxContent>
          <M.BoxBody>
            {props.type === 'language' ? (
              <M.Menucontainer>
                <M.Heading>Select Language</M.Heading>
                {languageList.map((key, i) => {
                  return (
                    <M.ListItem
                      onClick={() => {
                        props.fetchOverviewDetails(key.title)
                        setState('out')
                      }}
                      key={i}
                    >
                      {key.title}
                    </M.ListItem>
                  )
                })}
              </M.Menucontainer>
            ) : (
              <M.Menucontainer>
                <M.Heading>Select Type</M.Heading>
                {typeList.map((key, i) => {
                  return (
                    <M.ListItem
                      key={i}
                      onClick={() => {
                        props.fetchOverviewDetails(key.title)
                        setState('out')
                      }}
                    >
                      {key.title}
                    </M.ListItem>
                  )
                })}
              </M.Menucontainer>
            )}
          </M.BoxBody>
        </M.BoxContent>
      </M.BoxDialog>
      <M.Background onMouseDown={handleClick} ref={background} />
    </M.Modal>
  )
}

export default connect(
  null,
  {fetchOverviewDetails},
)(DropDown)
