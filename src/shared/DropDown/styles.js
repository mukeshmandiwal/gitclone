import styled from 'styled-components'

export const Modal = styled.div`
  position: absolute;
  margin-left: 1px;
  left: ${props => props.left};
  right: ${props => props.right};
  top: 100%;
  bottom: 0;
  display: flex;
  transition: opacity linear 0.15s;
  z-index: 1;
  width: 140px;
  height: ${props => props.modalSize};
  &.fade-in {
    opacity: 1;
    transition: opacity linear 0.15s;
  }
  &.fade-out {
    opacity: 0;
    transition: opacity linear 0.15s;
  }
`

export const Background = styled.div`
  position: fixed;
  display: block;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  outline: 0;
`

export const BoxDialog = styled.div`
  z-index: 1050;
  width: 100%;
  background-color: #fefefe;
  box-shadow: 0 3px 9px rgba(0, 0, 0, 0.5);
`

export const BoxContent = styled.div`
  width: 100%;
`
export const Menucontainer = styled.div`
  width: 300px;
  height: auto;
  max-height: 350px;
  margin: 4px 0 16px;
  font-size: 12px;
  background: #fff;
  border: 1px solid #d1d5da;
  border-radius: 3px;
  z-index: 1;
  display: flex;
  flex-direction: column;
  box-shadow: 0 1px 5px rgba(27, 31, 35, 0.15);
`

export const Heading = styled.div`
  padding: 10px;
  padding-left: 15px;
  background: #f2f2f2;
  font-weight: 600;
`

export const ListItem = styled.div`
  padding: 10px;
  padding-left: 15px;
  border-top: 1px solid #e1e4e8;
  &:hover {
    background: blue;
    color: #fff;
  }
`

export const BoxBody = styled.div`
  font-size: 14px;
  width: auto;
  display: flex;
  flex-direction: column;
`
