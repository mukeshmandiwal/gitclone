import React from 'react'

export const CompanyIcon = props => (
  <i
    className="material-icons"
    style={{
      fontSize: `${props.size}px`,
      color: `${props.color}`,
    }}
    {...props}
  >
    supervisor_account
  </i>
)

export const EmailIcon = props => (
  <i
    className="material-icons"
    style={{
      fontSize: `${props.size}px`,
      color: `${props.color}`,
    }}
    {...props}
  >
    mail_outline
  </i>
)

export const NewIcon = props => (
  <i
    className="material-icons"
    style={{
      fontSize: `${props.size}px`,
      color: `${props.color}`,
    }}
    {...props}
  >
    message
  </i>
)

export const ClearIcon = props => (
  <i
    className="material-icons"
    style={{
      fontSize: `${props.size}px`,
      color: `${props.color}`,
    }}
    {...props}
  >
    clear
  </i>
)

export const LocationIcon = props => (
  <i
    className="material-icons"
    style={{
      fontSize: `${props.size}px`,
      color: `${props.color}`,
    }}
    {...props}
  >
    where_to_vote
  </i>
)

export const StarIcon = props => (
  <i
    className="material-icons"
    style={{
      fontSize: `${props.size}px`,
      color: `${props.color}`,
    }}
    {...props}
  >
    star
  </i>
)

export const LanguageIcon = props => (
  <i
    className="material-icons"
    style={{
      fontSize: `${props.size}px`,
      color: `${
        props.color === 'JavaScript'
          ? 'rgba(255,255,0, 0.6)'
          : 'rgba(139, 0,0,0.4)'
      }`,
    }}
    {...props}
  >
    brightness_1
  </i>
)
